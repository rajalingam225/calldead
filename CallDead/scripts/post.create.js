﻿
$.urlParam = function (path, name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(path);
    if (results == null) {
        return null;
    }
    return decodeURI(results[1]) || 0;
};

$(document).ready(function () {
    $("[name=Tags]").tagify({ duplicates: false });

    $("#postLink").on("input propertychange paste", function () {
        var path = $.trim($(this).val());
        if (path.length > 0) {
            post.getVideoInfo(path);
        }
    });

    $("input[type=radio][name=SelectedType]").on("change", function () {
        var value = $(this).val();
        post.showHideVideoLink(value);
    });
        
    $(".published-date").datepicker({
        format: 'dd-mm-yyyy',
        startDate: '-1D',
        autoclose: true,
        todayBtn: true,
        todayHighlight: true
    }).datepicker("setDate", new Date());

    $(document).on("click", ".remove-featured", function (e) {
        e.preventDefault();
        $(this).parent().empty();
        $("#imageLink").val('');
    });
});


var post = {
    clearField: function () {        
        $("#Title").val('');
        $("#imageLink").val('');
        $("#description").val('');
        $("#dvFeaturedImage").html('');
    },
    showHideVideoLink: function (value) {
        this.clearField();
        $("#postLink").val('');
        if (value == 1) {            
            $(".postlink-section").show();
        } else {
            $(".postlink-section").hide();
        }
    },
    getVideoInfo: function (videoUrl) {
        this.clearField();
        var videoid = videoUrl;
        var matches = videoid.match(/^https?:\/\/www\.youtube\.com\/.*[?&]v=([^&]+)/i) || videoid.match(/^https?:\/\/youtu\.be\/([^?]+)/i);
        if (matches) {
            videoid = matches[1];
        }
        if (videoid.match(/^[a-z0-9_-]{11}$/i) === null) {
            return;
        }
        $.getJSON("https://www.googleapis.com/youtube/v3/videos", {
            key: "AIzaSyDRjElHjkVjz_iv81j5Xz_y-xZW6mzJb0k",
            part: "snippet,statistics",
            id: videoid
        }, function (data) {
            if (data.items.length === 0) {                
                return;
            }
            var imageUrl = data.items[0].snippet.thumbnails.medium.url;
            var title = data.items[0].snippet.title;
            var desc = data.items[0].snippet.description;
            var viewCount = data.items[0].statistics.viewCount;
            var likeCount = data.items[0].statistics.likeCount;
            var dislikeCount = data.items[0].statistics.dislikeCount;
            $("#Title").val(title);
            $("#imageLink").val(imageUrl);
            $("#description").val(desc);
            $("#dvFeaturedImage").html("<img src=\"" + imageUrl + "\" alt=\"thumbnailImage\" class=\"featured-image\" /> <span class=\"remove-featured\">x</span>");
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR.responseText || errorThrown)
        });
    }
}