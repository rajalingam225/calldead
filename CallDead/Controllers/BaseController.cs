﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CallDead.Controllers
{
    public class BaseController : Controller
    {
        public IHostingEnvironment hostEnv;

        private static string UploadFolderName = "upload";

        /// <summary>
        /// Method to download file from URI.
        /// </summary>
        /// <param name="requestUri"></param>
        /// <param name="webRootPath"></param>
        /// <returns></returns>
        public static async Task<string> DownloadAsync(Uri requestUri, string webRootPath)
        {
            string fileName = string.Empty;
            if (requestUri != null)
            {
                fileName = Guid.NewGuid().ToString() + Path.GetExtension(requestUri.AbsoluteUri);
                string uploadPath = GetUploadDirectory(webRootPath);
                string filePath = Path.Combine(uploadPath, fileName);
                using (var client = new HttpClient())
                using (var request = new HttpRequestMessage(HttpMethod.Get, requestUri))
                using (
                    Stream contentStream = await (await client.SendAsync(request)).Content.ReadAsStreamAsync(),
                    stream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None, Convert.ToInt32(contentStream.Length), true))
                {
                    await contentStream.CopyToAsync(stream);
                }
            }
            return fileName;
        }

        /// <summary>
        /// Method to download list of files from file upload.
        /// </summary>
        /// <param name="files"></param>
        /// <param name="webRootPath"></param>
        /// <returns></returns>
        public static async Task<List<string>> DownloadFile(List<IFormFile> files, string webRootPath)
        {
            List<string> fileList = new List<string>();
            foreach (var formFile in files)
            {
                fileList.Add(await DownloadFile(formFile, webRootPath));
            }
            return fileList;
        }

        /// <summary>
        /// Method to download file from file upload.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="webRootPath"></param>
        /// <returns></returns>
        public static async Task<string> DownloadFile(IFormFile file, string webRootPath)
        {
            string fileName = string.Empty;
            if (file.Length > 0)
            {
                fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                string uploadPath = GetUploadDirectory(webRootPath);
                string filePath = Path.Combine(uploadPath, fileName);
                using (FileStream stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }
            return fileName;
        }

        /// <summary>
        /// Method to create upload folder if not exists and returns the path.
        /// </summary>
        /// <param name="rootPath"></param>
        /// <returns></returns>
        public static string GetUploadDirectory(string rootPath)
        {
            string uploadFolderPath = Path.Combine(rootPath, UploadFolderName);
            bool exists = Directory.Exists(uploadFolderPath);
            if (!exists)
            {
                Directory.CreateDirectory(uploadFolderPath);
            }
            return uploadFolderPath;
        }
    }
}