﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;
using System.Linq;
using CallDead.Common;

namespace CallDead.Controllers
{
    public class PostController : BaseController
    {
        public PostController(IHostingEnvironment he)
        {
            hostEnv = he;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            // get all the post and sent to view for displaying it as table.
            return View();
        }

        public IActionResult Create()
        {
            Post vm = new Post
            {
                Type = new List<LookUp>
                {
                    new LookUp {Id = 1, Value = "Video"},
                    new LookUp {Id = 2, Value = "Post"}
                },
                Categories = new List<LookUp>
                {
                    new LookUp {Id = 1, Value = "UnCategorized"},
                    new LookUp {Id = 2, Value = "Design"},
                    new LookUp {Id = 3, Value = "Development"},
                    new LookUp {Id = 4, Value = "Writing"},
                    new LookUp {Id = 5, Value = "Books"}
                },
                Visibility = new List<LookUp>
                {
                    new LookUp {Id=2, Value="Editor" },
                    new LookUp {Id=3, Value="X-Member" },
                    new LookUp {Id=4, Value="Subscriber" }
                },
                PostDate = DateTime.Now
            };
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Post pst, string submit, IFormFile image)
        {
            pst.PostDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                // if the post type is video, download file and set featured image as downloaded file name.                
                LookUp selected = pst.Type.Where(p => p.Value.ToLower() == "video").FirstOrDefault();
                if(selected != null && pst.SelectedType == selected.Id)
                {
                    string fileName = string.Empty;
                    if (!string.IsNullOrWhiteSpace(pst.imageLink))
                    {
                        Uri linkURI = new Uri(pst.imageLink);
                        fileName = await DownloadAsync(linkURI, hostEnv.WebRootPath);
                    }
                    if (string.IsNullOrWhiteSpace(fileName))
                    {
                        fileName = await DownloadFile(image, hostEnv.WebRootPath);
                    }
                    pst.featuredImageName = fileName;
                }
                else
                {
                    pst.featuredImageName = string.Empty;
                }
                
                switch (submit.ToLower())
                {
                    case "draft":
                        pst.IsPublished = false;
                        break;
                    case "publish":
                        pst.IsPublished = true;
                        pst.PublishedDate = DateTime.Now;
                        break;
                }
                
                //db.AddToMovies(pst);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pst);
        }
    }
}
