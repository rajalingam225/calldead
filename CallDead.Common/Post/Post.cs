﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CallDead.Common
{
    public class Post
    {
        public int Id { get; set; }

        [Required]
        [StringLength(500, MinimumLength = 3)]
        public string Title { get; set; }

        public List<LookUp> Type { get; set; }

        public string Link { get; set; }

        public string Description { get; set; }

        public string imageLink { get; set; }

        public string MetaData { get; set; }

        public List<LookUp> Categories { get; set; }

        public List<LookUp> Visibility { get; set; }

        public string Tags { get; set; }

        [Display(Name = "Trending")]
        public bool IsTrending { get; set; }

        [Display(Name = "Active")]
        public bool IsActive { get; set; }

        [Display(Name = "Comment")]
        public bool CanComment { get; set; }

        [Display(Name = "Schedule Date")]
        public DateTime ScheduleDate { get; set; }

        [Display(Name = "Post Date")]
        public DateTime PostDate { get; set; }

        public bool IsPublished { get; set; }

        [Display(Name = "Published Date"), DisplayFormat(DataFormatString = "{0:dd/MMM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime PublishedDate { get; set; }

        public int SelectedType { get; set; }

        public string featuredImageName { get; set; }
    }
}
