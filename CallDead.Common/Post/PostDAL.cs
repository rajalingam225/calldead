﻿using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;

namespace CallDead.Common
{
    public class PostDAL : BaseDAL
    {
        public List<Post> GetAllPosts()
        {
            List<Post> pstList = new List<Post>();
            using (MySqlConnection con = GetBaseConnection())
            {
                try
                {
                    con.Open();
                    using (MySqlCommand cmd = new MySqlCommand("procGetAllPosts", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        MySqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            pstList.Add(LoadFromReader(reader));
                        }
                        reader.Close();
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return pstList;
        }

        public Post GetPost(int id)
        {
            Post pst = new Post();            
            using (MySqlConnection con = GetBaseConnection())
            {
                try
                {
                    con.Open();
                    using (MySqlCommand cmd = new MySqlCommand("procGetPost", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PostId", id);
                        MySqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            pst = LoadFromReader(reader);
                        }
                        reader.Close();
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return pst;
        }

        private Post LoadFromReader(MySqlDataReader reader)
        {
            Post pst = new Post();
            SmartDataReader sdr = new SmartDataReader(reader);
            pst.Title = sdr.GetString("Title");            
            return pst;
        }
    }
}
