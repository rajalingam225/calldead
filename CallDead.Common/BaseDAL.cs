﻿using MySql.Data.MySqlClient;

namespace CallDead.Common
{
    public class BaseDAL : CallDeadContext
    {
        public BaseDAL() : base(Constants.DefaultConnection)
        {

        }

        public MySqlConnection GetBaseConnection()
        {
            return base.GetConnection();
        }
    }
}
