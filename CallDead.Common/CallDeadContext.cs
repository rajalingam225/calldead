﻿using MySql.Data.MySqlClient;

namespace CallDead.Common
{
    public class CallDeadContext
    {
        public string ConnectionString { get; set; }

        public CallDeadContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }
    }
}
