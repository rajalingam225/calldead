﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace CallDead.Common
{
    public class SmartDataReader
    {
        private List<string> columnNames;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="reader"></param>
		public SmartDataReader(MySqlDataReader reader)
        {
            this.reader = reader;
            columnNames = new List<string>();

            for (int i = 0; i < reader.FieldCount; i++)
            {
                columnNames.Add(reader.GetName(i));
            }

        }

        /// <summary>
        /// Performs a quick lookup on the passed in datareader schema to determine if specific columnName exists
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public bool ColumnExists(string columnName)
        {
            return columnNames.Contains(columnName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sdrData"></param>
        /// <param name="columnName"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private T GetValueOrDefault<T>(MySqlDataReader sdrData, string columnName, T defaultValue)
        {
            object value = defaultValue;

            if (ColumnExists(columnName))
            {
                if (!sdrData.IsDBNull(sdrData.GetOrdinal(columnName)))
                {
                    value = sdrData[columnName];
                }
            }

            return (T)value;
        }


        /// <summary>
        /// Get value as Integer
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public int GetInteger(String column)
        {
            return GetValueOrDefault<int>(reader, column, 0);
        }

        /// <summary>
        /// Get value as Int32
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
		public int GetInt32(string column)
        {
            return GetValueOrDefault<int>(reader, column, 0);
        }

        /// <summary>
        /// Get value as Int64
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
		public Int64 GetInt64(String column)
        {
            return GetValueOrDefault<Int64>(reader, column, 0);
        }

        /// <summary>
        /// Get value as Int16
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public short GetInt16(String column)
        {
            return GetValueOrDefault<short>(reader, column, (short)0);
        }

        /// <summary>
        /// Get value as Byte
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public short GetByte(String column)
        {
            return GetValueOrDefault<byte>(reader, column, (byte)0);
        }

        /// <summary>
        /// Get value as Byte()
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public byte[] GetByteArray(String column)
        {
            return GetValueOrDefault<byte[]>(reader, column, new byte[0]);
        }

        /// <summary>
        /// Get value as Float
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
		public float GetFloat(string column)
        {
            return GetValueOrDefault<float>(reader, column, 0);
        }

        // this doesn't work, so commented out on 4/09/2008 by Angus
        // Sam: this is used by eStore, cannot be commented out
        /// <summary>
        /// Get value as Double
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public double GetDouble(string column)
        {
            return GetValueOrDefault<double>(reader, column, 0);
        }

        /// <summary>
        /// Get value as Decimal
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
		public decimal GetDecimal(string column)
        {
            return GetValueOrDefault<decimal>(reader, column, (decimal)0);
        }

        /// <summary>
        /// Get value as Boolean
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
		public bool GetBoolean(string column)
        {
            return GetValueOrDefault<bool>(reader, column, false);
        }

        /// <summary>
        /// Get value as Boolean (if no value found, return Default Value)
        /// </summary>
        /// <param name="column"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
		public bool GetBoolean(string column, bool defaultValue)
        {
            return GetValueOrDefault<bool>(reader, column, defaultValue);
        }

        /// <summary>
        /// Get value as String
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
		public string GetString(string column)
        {
            return GetValueOrDefault<string>(reader, column, "");
        }

        /// <summary>
        /// Get value as Object
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
		public object GetObject(string column)
        {
            return GetValueOrDefault<object>(reader, column, null);
        }

        /// <summary>
        /// Get a datetime from SQL
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public DateTime GetDateTime(string column)
        {
            return GetValueOrDefault<DateTime>(reader, column, new DateTime(1900, 1, 1));
        }

        /// <summary>
        /// Get value as Guid
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public Guid GetGuid(string column)
        {
            return GetValueOrDefault<Guid>(reader, column, new Guid());
        }

        /// <summary>
        /// Perform Read
        /// </summary>
        /// <returns></returns>
		public bool Read()
        {
            return this.reader.Read();
        }

        /// <summary>
        /// Close reader
        /// </summary>
		public void Close()
        {
            reader.Close();
            reader = null;
        }

        private MySqlDataReader reader;
    }
}
