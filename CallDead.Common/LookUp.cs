﻿using System;

namespace CallDead.Common
{
    public class LookUp
    {
        public int Id { set; get; }
        public string Value { set; get; }
        public bool Selected { get; set; }
    }
}
